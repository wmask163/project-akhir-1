<!DOCTYPE html>
<html>
    <head>
        <title>Facebook - The Social Network</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{asset('template2/assets/css/style.css')}}"/>
        <link rel="stylesheet" href="{{asset('template2/assets/css/admin.css')}}"/>
        <style>
            .box{
                background: rgba(255,255,255,1);
                padding: 10px 20px;
                border-radius: 2px;
                box-shadow: 0px 0px 15px 5px rgba(0,0,0,0.4);
            }
        </style>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-8894073675575663",
            enable_page_level_ads: true
        });
        </script>
    </head>
    <body>
        @include('utama.headerdashboard')
        @include('utama.maindashboard')
        @include('utama.footerdashboard')
        <script>
            function like(id){
                var elem = document.getElementById("post_like_count_"+id);
                var count = parseInt(elem.innerHTML);
                elem.innerHTML = count+1;
                highlight(elem);
            }
            function share(id){
                var elem = document.getElementById("post_share_count_"+id);
                var count = parseInt(elem.innerHTML);
                elem.innerHTML = count+1;
                highlight(elem);
            }
            function comment(id){
                var elem = document.getElementById("post_comment_count_"+id);
                var count = parseInt(elem.innerHTML);
                elem.innerHTML = count+1;
                highlight(elem);
            }
            function highlight(elem){
                elem.style.color = "red";
                elem.parentElement.parentElement.style.transform="scale(1.5)";
                setTimeout(function(){
                    elem.style.color="";
                    elem.parentElement.parentElement.style.transform="scale(1)";
                },300);
            }
        </script>
    </body>
</html>
