<!DOCTYPE html>
<html>
    <head>
        <title>Facebook - The Social Network</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{asset('template2/assets/css/style.css')}}"/>
        <style>
            .signup-form{
                background: rgba(255,255,255,1);
                padding: 10px 20px;
                border-radius: 2px;
                box-shadow: 0px 0px 15px 5px rgba(0,0,0,0.4);
            }
        </style>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-8894073675575663",
    enable_page_level_ads: true
  });
</script>
    </head>
    <body>
        @include('profile.headerlogin')
        @include('profile.mainlogin')
        @include('profile.footerlogin')
    </body>
</html>
