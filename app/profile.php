<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['nama', 'tanggal_lahir', 'nomor_ponsel'];
}
